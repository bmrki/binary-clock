let hour_tens = document.querySelector('.hour-tens');
let hour_ones = document.querySelector('.hour-ones');
let min_tens = document.querySelector('.min-tens');
let min_ones = document.querySelector('.min-ones');
let sec_tens = document.querySelector('.sec-tens');
let sec_ones = document.querySelector('.sec-ones');

function start() {
    setInterval(function () {
        let date = new Date();
        setBinarySec(date);
        setBinaryHour(date);
        setBinaryMin(date);
        // showTime(date);
    }, 1000);
}

function setBinaryHour(date) {
    let hour = date.getHours();
    let tens = Math.floor(hour / 10);
    let ones = hour % 10;

    let bin_ones = ones.toString(2).split('');
    unshiftWithZero(bin_ones, hour_ones.children.length)
    let bin_tens = tens.toString(2).split('');
    unshiftWithZero(bin_tens, hour_tens.children.length)

    toggleActiveClass(hour_tens, bin_tens);
    toggleActiveClass(hour_ones, bin_ones);
}

function setBinaryMin(date) {
    let min = date.getMinutes();
    let tens = Math.floor(min / 10);
    let ones = min % 10;

    let bin_ones = ones.toString(2).split('');
    unshiftWithZero(bin_ones, min_ones.children.length)
    let bin_tens = tens.toString(2).split('');
    unshiftWithZero(bin_tens, min_tens.children.length)

    toggleActiveClass(min_tens, bin_tens);
    toggleActiveClass(min_ones, bin_ones);
}

function setBinarySec(date) {
    let sec = date.getSeconds();
    let tens = Math.floor(sec / 10);
    let ones = sec % 10;

    let bin_ones = ones.toString(2).split('');
    unshiftWithZero(bin_ones, sec_ones.children.length)
    let bin_tens = tens.toString(2).split('');
    unshiftWithZero(bin_tens, sec_tens.children.length)

    toggleActiveClass(sec_tens, bin_tens);
    toggleActiveClass(sec_ones, bin_ones);
}

function unshiftWithZero(arr, n) {
    while (arr.length < n) {
        arr.unshift('0');
    }
}

function toggleActiveClass(pointers, binArr) {
    for (let i = pointers.children.length - 1; i >= 0; i--) {
        pointers.children[i].className = '';
        if (binArr[i] === '1') {
            pointers.children[i].classList.add('ptr-active');
        } else {
            pointers.children[i].classList.add('ptr-not-active');
        }
    }
}

function showTime(date) {
    console.log(
        date.getHours() +
        " : " + date.getMinutes() +
        " : " + date.getSeconds());
}