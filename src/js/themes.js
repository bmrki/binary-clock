const themes = {
    1:'./src/css/default_theme.css',
    2:'./src/css/dark_theme.css',
    3:'./src/css/light_theme.css',
    4:'./src/css/red_theme.css'
};

let active_theme = 1;

document.querySelector('.right').onclick = function () {
    active_theme++;
    console.log(Object.keys(themes).length);
    if (active_theme > Object.keys(themes).length) {
        active_theme = 1;
    }
    document.querySelector('#link').href = themes[active_theme];
}

document.querySelector('.left').onclick = function () {
    active_theme--;
    if (active_theme < 1) {
        active_theme = Object.keys(themes).length;
    }
    document.querySelector('#link').href = themes[active_theme];
}